function Todo(id, task, who, dueDate) {
    this.id = id;
    this.task = task;
    this.who = who;
    this.dueDate = dueDate;
    this.done = false;
}

var todos = new Array();

window.onload = init;

function init() {
    var submitButton = document.getElementById("submit");
    submitButton.onclick = getFormData;

    getTodoItems();
	console.log(todos);
}

function getTodoItems() {
    if (localStorage) {
        for (var i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            if (key.substring(0, 4) == "todo") {
                var item = localStorage.getItem(key);
                var todoItem = JSON.parse(item);
                todos.push(todoItem);
           }
        }
        addTodosToPage();
    }
    else {
        console.log("Error: you don't have localStorage!");
    }
}


function addTodosToPage() {
    var ul = document.getElementById("todoList");
    var listFragment = document.createDocumentFragment();
    for (var i = 0; i < todos.length; i++) {
        var todoItem = todos[i];
        var li = createNewTodo(todoItem);
        listFragment.appendChild(li);
    }
    ul.appendChild(listFragment);
}
function addTodoToPage(todoItem) {
    var ul = document.getElementById("todoList");
    var li = createNewTodo(todoItem);
    ul.appendChild(li);
    document.forms[0].reset();
}

function createNewTodo(todoItem) {
    var li = document.createElement("li");
    var spanTodo = document.createElement("span");
    spanTodo.innerHTML =
        todoItem.who + " needs to " + todoItem.task + " by " + todoItem.dueDate;

    var spanDone = document.createElement("span");
    if (!todoItem.done) {
        spanDone.setAttribute("class", "notDone");
        spanDone.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		spanDone.onclick = updateDone;
    }
    else {
        spanDone.setAttribute("class", "done");
        spanDone.innerHTML = "&nbsp;&#10004;&nbsp;";
		spanDone.onclick = updateDone;
    }

    li.appendChild(spanDone);
    li.appendChild(spanTodo);
	
    return li;
}
             
function getFormData() {
    var task = document.getElementById("task").value;
    if (checkInputText(task, "Please enter a task")) return;

    var who = document.getElementById("who").value;
    if (checkInputText(who, "Please enter a person to do the task")) return;

    var date = document.getElementById("dueDate").value;
    if (checkInputText(date, "Please enter a due date")) return;
    
    var id = todos.length;
    var todoItem = new Todo(id, task, who, date);
    todos.push(todoItem);
    addTodoToPage(todoItem);
    saveTodoItem(todoItem);
}

function checkInputText(value, msg) {
    if (value == null || value == "") {
        alert(msg);
        return true;
    }
    return false;
}

function saveTodoItem(todoItem) {
    if (localStorage) {
        var key = "todo" + todoItem.id;
        var item = JSON.stringify(todoItem);
        localStorage.setItem(key, item);
    }
    else {
        console.log("Error: you don't have localStorage!");
    }
}  

function updateDone(event){
	var isdone  = event.target.parentNode.childNodes[0].className;
	if (isdone =='done'){
		event.target.parentNode.childNodes[0].className = 'notDone';		
		var content = event.target.parentNode.childNodes[1].innerHTML;
		var ind = '';		
		var item = null;
		
		//find entry in local storage
		for (key in localStorage){
			entry = JSON.parse(localStorage[key]);
			console.log(entry);
			console.log(content);
			console.log(entry.who + " needs to " + entry.task + " by " + entry.dueDate);
			if (entry.who + " needs to " + entry.task + " by " + entry.dueDate == content){
				ind = key;
				item = entry;
			}
		}
		console.log(ind);
		
		//update entry in local storage
		var arrind = ind.replace('todo','');
		todos[arrind] = {id: item.id, task: item.task, who: item.who, dueDate: item.dueDate, done: false};
		console.log(todos);
		localStorage.setItem(ind, JSON.stringify({id: item.id, task: item.task, who: item.who, dueDate: item.dueDate, done: false}));
		location.reload();
	}
	else{
		event.target.parentNode.childNodes[0].className = 'done';
		var content = event.target.parentNode.childNodes[1].innerHTML;
		var ind = '';
		var item = null;
		
		//find entry in local storage
		for (key in localStorage){
			entry = JSON.parse(localStorage[key]);
			console.log(entry);
			console.log(content);
			console.log(entry.who + " needs to " + entry.task + " by " + entry.dueDate);
			if (entry.who + " needs to " + entry.task + " by " + entry.dueDate == content){
				ind = key;
				item = entry;
			}
		}
		console.log(ind);
		var arrind = ind.replace('todo','');
		todos[arrind] = {id: item.id, task: item.task, who: item.who, dueDate: item.dueDate, done: false};
		
		//update entry in local storage
		localStorage.setItem(ind, JSON.stringify({id: item.id, task: item.task, who: item.who, dueDate: item.dueDate, done: true}));
		location.reload();
	}
	
}

